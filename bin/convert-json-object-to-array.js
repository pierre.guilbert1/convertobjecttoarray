#!/usr/bin/env node

// wget http://localhost:3000/api/metrics -O metrics.json
// then node tools/convert-json-to-definition.js metrics.json -i metrics.json -o src/

const yargs = require('yargs');
const fs = require('fs');

(function main() {
    const argv = yargs
        .option('input', {
            alias: 'i',
            description: 'path to a valid json file',
            type: 'string',
        })
        .option('output', {
            alias: 'o',
            description: 'path to a valid output path',
            type: 'string',
        })
        .help()
        .alias('help', 'h').argv;

    if (argv.input && argv.output) {
        // read file in argv.input

        fs.readFile(`${argv.input}`, (err, data) => {
            if (err) throw err;
            const result = JSON.stringify(Object.values(JSON.parse(data)));

            fs.writeFile(`${argv.output}`, result, (err) => {
                if (err) throw err;
                console.log(`Data from ${argv.input} written to file ${argv.output}`);
            });
        });
    } else {
        console.log(`You should have a --input and --output option`);
    }
})();
