# Scripts

# "convert:object"

Convert the core-features json which is an object to an array so it is easier to import in mongo

# "import:toMongo"

Import directly the json array to mongo db collection

You still need to add the datasetId to all documents

```json
db.trendsFromJson.update(
  {},
  { "$set": { "datasetId": "x" } },
  { "multi": true }
)
```
